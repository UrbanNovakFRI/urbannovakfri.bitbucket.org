/* global L, distance */


// seznam z markerji na mapi
var polys = [];

var mapa;


const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {
  var json = pridobiPodatke();
  
  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };
  
  
  
  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
  
  var json = pridobiPodatke(function(json){
    json.features.forEach(function(value) {
      try {
        var poly = null;
        if(value.geometry.coordinates.length == 2) {
          poly = L.polygon([fix(value.geometry.coordinates[0]), fix(value.geometry.coordinates[1])], {color: 'blue'});
        }
        else {
          poly = L.polygon(fix(value.geometry.coordinates[0]), {color: 'blue'});
        }
        
        
        
        polys.push(poly);
        
        poly.addTo(mapa);
        
        poly.bindPopup(
          "<div>" + value.properties.name + "</div>" +
          "<div>" + value.properties["addr:street"] + " " + value.properties["addr:housenumber"] + "</div>"
        );
        
      }
      catch(e){
        console.log(e);
      }
    });
    
    
  });
  
  function mapClick(e) {
    var latlng = e.latlng;
    
    for(var a = 0; a < polys.length; a++) {
      mapa.removeLayer(polys[a]);
    }
    polys = [];
    
    var json = pridobiPodatke(function(json){
      //console.log("loop?");
      json.features.forEach(function(value) {
        try {
          var poly = null;
          if(value.geometry.coordinates.length == 2) {
            poly = L.polygon([fix(value.geometry.coordinates[0]), fix(value.geometry.coordinates[1])], {color: 'blue'});
          }
          else {
            poly = L.polygon(fix(value.geometry.coordinates[0]), {color: 'blue'});
          }
          
          
          
          poly.bindPopup(
            "<div>" + value.properties.name + "</div>" +
            "<div>" + value.properties["addr:street"] + " " + value.properties["addr:housenumber"] + "</div>"
          );
          
          
          
          poly.addTo(mapa);
          
          
          
          
          var center = poly.getCenter();
          
          var rad = 1;
          
          if(distance(center.lat, center.lng, latlng.lat, latlng.lng, 'K') <= rad) {
            
            mapa.removeLayer(poly);
            
            
            if(value.geometry.coordinates.length == 2) {
              poly = L.polygon([fix(value.geometry.coordinates[0]), fix(value.geometry.coordinates[1])], {color: 'green'});
            }
            else {
              poly = L.polygon(fix(value.geometry.coordinates[0]), {color: 'green'});
            }
            
            
            poly.bindPopup(
              "<div>" + value.properties.name + "</div>" +
              "<div>" + value.properties["addr:street"] + " " + value.properties["addr:housenumber"] + "</div>"
            );
          
            //polys.push(poly);
            
            poly.addTo(mapa);
            
            
          }
          polys.push(poly);
          
        }
        catch(e){
          
        }
      });
      
      
    });
    
    mapa.zoomIn(0.00000000000001);
    
    
  }
  
  mapa.on('click', mapClick);
  
  
  function spaghett() {
    for(var a = 0; a < polys.length; a++) {
      mapa.removeLayer(polys[a]);
      polys[a].addTo(mapa);
    }
    
  }
  
  document.getElementById("mapa_id").addEventListener("wheel", spaghett);

});


function fix(coords) {
  var fixed = [];
  
  for(var a = 0; a < coords.length; a++) {
    fixed.push([coords[a][1], coords[a][0]]);
  }
  
  return(fixed);
}


/**
 * Za podano vrsto interesne točke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 * 
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        // nastavimo ustrezna polja (število najdenih zadetkov)
        
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah,
 * z dodatnim opisom, ki se prikaže v oblačku ob kliku in barvo
 * ikone, glede na tip oznake (FRI = rdeča, druge fakultete = modra in
 * restavracije = zelena)
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 * @param tip "FRI", "restaurant" ali "faculty"
 */
function dodajMarker(lat, lng, opis, inRange) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-' + 
      (inRange ? 'green' : 'blue') + 
      '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  //marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  //markerji.push(marker);
  
}


/**
 * Preveri ali izbrano oznako na podanih GPS koordinatah izrišemo
 * na zemljevid glede uporabniško določeno vrednost radij, ki
 * predstavlja razdaljo od FRI.
 * 
 * Če radij ni določen, je enak 0 oz. je večji od razdalje izbrane
 * oznake od FRI, potem oznako izrišemo, sicer ne.
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 */
function prikaziOznako(lng, lat) {
  return true;
}

